package factoryDevices;

public class FactoryDevice {

    public enum DeviceType{
        ANDROID,
        IOS,
        CLOUDANDROID
    }
    public static IDevice make(DeviceType type) {
        IDevice iDevice;
        switch (type) {
            case ANDROID:
                iDevice = new Android();
                break;
            case IOS:
                iDevice = new IOS();
                break;
            case CLOUDANDROID:
                iDevice = new BrowserStackAndroid();
                break;
            default:
                iDevice = new Android();
                break;

        }
        return iDevice;
    }
}
