package activities.whenDo;

import controlsAppium.Button;
import controlsAppium.TextBox;
import org.openqa.selenium.By;

public class CreateTask {
    public TextBox titleBox = new TextBox(By.id("com.vrproductiveapps.whendo:id/noteTextTitle"));
    public TextBox bodyBox = new TextBox(By.id("com.vrproductiveapps.whendo:id/noteTextNotes"));
    public Button saveButton = new Button(By.id("com.vrproductiveapps.whendo:id/saveItem"));

    public CreateTask(){}
}
