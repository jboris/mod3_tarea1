package activities.whenDo;

import controlsAppium.Button;
import controlsAppium.Label;
import org.openqa.selenium.By;

public class TaskList {
    public Button addTaskButton = new Button(By.id("com.vrproductiveapps.whendo:id/fab"));
    public Label nameLabel = new Label(By.id("com.vrproductiveapps.whendo:id/home_list_item_text"));
    public Label bodyContent = new Label(By.id("com.vrproductiveapps.whendo:id/home_list_item_text2"));

    public TaskList() {}
}
