import activities.whenDo.CreateTask;
import activities.whenDo.DeleteConfirmDialog;
import activities.whenDo.TaskList;
import activities.whenDo.UpdateTask;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sessions.Session;

import java.net.MalformedURLException;

public class WhenDoTest {
    private TaskList taskList = new TaskList();
    private CreateTask createTask = new CreateTask();
    private UpdateTask updateTask = new UpdateTask();
    private DeleteConfirmDialog deleteConfirmDialog = new DeleteConfirmDialog();

    @Test
    public void test_task_create() throws MalformedURLException {
        taskList.addTaskButton.click();
        String title = "Test";
        createTask.titleBox.setValue(title);
        String body = "Task body.";
        createTask.bodyBox.setValue(body);
        createTask.saveButton.click();

        Assertions.assertEquals(title, taskList.nameLabel.getText(), "Creación incorrecta.");
    }

    @Test
    public void test_update_task() throws MalformedURLException {
        taskList.addTaskButton.click();
        String title = "Test";
        createTask.titleBox.setValue(title);
        String body = "Task body.";
        createTask.bodyBox.setValue(body);
        createTask.saveButton.click();

        taskList.nameLabel.click();
        updateTask.titleBox.setValue(title.toUpperCase());
        updateTask.bodyBox.setValue(body.toUpperCase());
        updateTask.saveButton.click();

        Assertions.assertEquals(title.toUpperCase(), taskList.nameLabel.getText(), "Actualización incorrecta.");
    }

    @Test
    public void test_delete_task() throws MalformedURLException {
        taskList.addTaskButton.click();
        String title = "Test";
        createTask.titleBox.setValue(title);
        String body = "Task body.";
        createTask.bodyBox.setValue(body);
        createTask.saveButton.click();

        taskList.nameLabel.click();
        updateTask.deleteButton.click();
        deleteConfirmDialog.deleteButton.click();

        Assertions.assertEquals(0, taskList.nameLabel.count(), "Eliminación incorrecta.");
    }

    @AfterEach
    public void close() throws MalformedURLException {
        Session.getInstance().close();
    }
}
