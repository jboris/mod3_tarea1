package sessions;

import controlsAppium.Label;
import factoryDevices.FactoryDevice;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;

import java.net.MalformedURLException;
import java.time.Duration;

public class Session {

    // tener un atributo del mismo tipo de la clase.
    private static Session instance = null;
    private AppiumDriver driver;

    // tener un constructor privado.
    private Session() throws MalformedURLException {
        driver = FactoryDevice.make(FactoryDevice.DeviceType.ANDROID).create();
        //driver = FactoryDevice.make(FactoryDevice.DeviceType.CLOUDANDROID).create();
    }

    //tener un metodo estático público que retorne la instancia única.
    public static Session getInstance() throws MalformedURLException {
        if (instance == null)
            instance = new Session();
        return instance;
    }

    public void close() {
        driver.quit();
        instance = null;
    }

    public AppiumDriver getDriver() {
        return driver;
    }


    public void swipe() throws MalformedURLException {
        TouchAction action = new TouchAction(driver);
        Label start = new Label(By.id("com.google.android.apps.nexuslauncher:id/all_apps_header"));
        Label finish = new Label(By.id("com.google.android.apps.nexuslauncher:id/search_container_workspace"));
        int startX = start.getControl().getLocation().getX();
        int startY = start.getControl().getLocation().getY();
        int finishX = finish.getControl().getLocation().getX();
        int finishY = finish.getControl().getLocation().getY();

        action.press(PointOption.point(startX, startY))
              .waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1)))
              .moveTo(PointOption.point(finishX, finishY))
              .release()
              .perform();




    }
}
